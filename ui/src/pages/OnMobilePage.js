import React from 'react';
//import '../styles/style.css';

class OnMobilePage extends React.PureComponent{
  render() {
    return (
      <>
        <main>
          <div className="cotainer">
            <div className="row justify-content-center">
              <div className="col-md-12">
                <div className="account-wall">
                  <div className="steps landing__section">
                    <div className="container how-to-play">
                      <div className="steps__inner steps-combos how-to-play">
                        <div className="step step-combos how-to-play">
                          <h2 className="weight-500 mb-1 phoneNumbefrem text-white">
                            <b> Jinsi ya kushiriki bila kuingia mtandaoni (SIMU)</b>
                          </h2>
                          <br/>
                          <table className="combos">
                            <tbody>
                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        1
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Bonyeza *150*00#
                              </td>
                            </tr>

                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        2
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Chagua namba 4 (Lipa kwa m-pesa)
                              </td>
                            </tr>

                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        3
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Chagua 4 kisha weka namba ya kampuni (222223)
                              </td>
                            </tr>


                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        4
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Ingiza namba ya kumbukumbu (123)
                              </td>
                            </tr>

                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        5
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Endelea
                              </td>
                            </tr>

                            <tr>
                              <td className="tac">
                                <div className="circle combo">
                                  <div className="circle__inner">
                                    <div className="circle__wrapper">
                                      <div className="circle__content">
                                        6
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                              <td className="frequency tac h5 mb-2 weight-300 questions">
                                Ingiza namba yako ya Siri
                              </td>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </main>
      </>
    );

  }
}

export default OnMobilePage;
