import React from 'react';
//import '../styles/style.css';

class ContactPage extends React.PureComponent{
  render() {
    return (
      <>
        <main className="login-form">
          <div className="cotainer">
            <div className="row justify-content-center">
              <div className="col-md-8">
                <div className="account-wall">
                  <div className="container">
                    <div className="row">
                      <div className="col-md-12">
                        <div id="upMain">
                          <div style={{margin:'10px'}}>
                            <article>
                              <div className="row">
                                <div className="col-sm-12">
                                  <div className="contact-info">
                                    <div className="section-title">
                                      <h2 className="weight-500 mb-1 phoneNumber">
                                        <b>Wasiliana nasi</b>
                                      </h2>
                                    </div>
                                    <br/>
                                    <h4 className="h4 mb-2 weight-300 questions">Mikocheni Dar es salaam</h4>
                                    <p className="h4 mb-2 weight-300 questions">Mwai kibaki Road</p>
                                    <p className="h4 mb-2 weight-300 questions">Email: help@bongomilionea.co.tz</p>
                                    <p className="h4 mb-2 weight-300 questions">255 (0) 659 070 070</p>
                                  </div>
                                  <br/><br/> <br/><br/><br/>
                                </div>
                              </div>
                            </article>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      </>
    );

  }
}

export default ContactPage;
