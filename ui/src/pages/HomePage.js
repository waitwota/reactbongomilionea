import React from 'react';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
//import '../styles/style.css';
import Header from "../components/Header";
import Footer from "../components/Footer";
import Testing from "../components/Testing";

class HomePage extends React.PureComponent{

  render() {
    return (
        <>
          <div>

            <Header/>
            <Testing/>

          </div>

          <Footer/>
        </>
    );

  }
}

export default HomePage;
