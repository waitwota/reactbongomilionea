import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import MainLayout from "./components/MainLayout";
import PageLoader from "./components/PageLoader";
import ScrollToTop from 'react-router-scroll-top';

import HomePage from "./pages/HomePage";
//const HomePage = React.lazy(() => import('./pages/HomePage'));
const AboutPage = React.lazy(() => import('./pages/AboutPage'));
const ContactPage = React.lazy(() => import('./pages/ContactPage'));
const FaqPage = React.lazy(() => import('./pages/FaqPage'));
const OnMobilePage = React.lazy(() => import('./pages/OnMobilePage'));
const OnWebPage = React.lazy(() => import('./pages/OnWebPage'));
const PolicyPage = React.lazy(() => import('./pages/PolicyPage'));
const TermsPage = React.lazy(() => import('./pages/TermsPage'));

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {

  render() {

    return (
      <BrowserRouter basename={getBasename()}>
        <ScrollToTop>
          <Switch>
            <Route exact path="/" component={HomePage}/>
            <MainLayout>

              <React.Suspense fallback={<PageLoader/>}>
                <Route exact path="/about" component={AboutPage} />
                <Route exact path="/contact" component={ContactPage} />
                <Route exact path="/faq" component={FaqPage} />
                <Route exact path="/onMobile" component={OnMobilePage} />
                <Route exact path="/onWeb" component={OnWebPage} />
                <Route exact path="/policy" component={PolicyPage} />
                <Route exact path="/terms" component={TermsPage} />
              </React.Suspense>

            </MainLayout>
          </Switch>
        </ScrollToTop>
      </BrowserRouter>
    );
  }
}

export default App;
