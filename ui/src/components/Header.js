import headerLogo from '../images/simba.png';
import React from 'react';
import {Link} from "react-router-dom";
/*
import {gPreventDefault} from "../../utils/preventDef";
*/

class Header extends React.PureComponent {

  render() {
    return (

      <nav className="navbar navbar-expand-lg navbar-light navbar-laravel">
        <div className="container">
          <Link to="/" className="navbar-brand">
            <img src={headerLogo} alt="logo" title="logo"/>
          </Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"> </span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to="/" className="nav-link">Nyumbani</Link>
              </li>
              <li className="nav-item">
                <Link to="/about" className="nav-link">Kuhusu sisi</Link>
              </li>
              <li className="nav-item dropdown dmenu">
                <a className="nav-link dropdown-toggle" href="#/" id="navbardrop" data-toggle="dropdown">
                  Jinsi ya kushiriki
                </a>
                <div className="dropdown-menu sm-menu">
                  <Link to="/onmobile" className="dropdown-item">Bila mtandao <i className="fa fa-mobile"> </i> (SIMU)</Link>
                  <Link to="/onweb" className="nav-link">Mtandaoni <i className="fa fa-globe"> </i> (WEB)</Link>
                </div>
              </li>
              <li className="nav-item">
                <Link to="/mamilionea" className="nav-link">Mamilionea</Link>
              </li>
              <li className="nav-item">
                <Link to="/contact" className="nav-link">Mawasiliano</Link>
              </li>
            </ul>

          </div>
        </div>
      </nav>
    );
  }
}

export default Header;
