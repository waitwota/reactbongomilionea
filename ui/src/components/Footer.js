import footerLogo from '../images/gamingtz.png';
import React from 'react';
import {Link} from "react-router-dom";
/*
import {gPreventDefault} from "../../utils/preventDef";
*/

class Footer extends React.PureComponent {

  render() {
    return (

      <div className="copyright">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <img src={footerLogo} className="gamming" alt="logo" title="logo"/>
              <span>Copyright © 2021, All Right Reserved </span>
            </div>
            <div className="col-md-6">
              <div className="copyright-menu">
                <ul>
                  <li>
                    <Link to="/terms">Vigezo na Masharti</Link>
                  </li>
                  <li>
                    <Link to="/policy">Sera</Link>
                  </li>
                  <li>
                    <Link to="/contact">Wasiliana nasi</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
