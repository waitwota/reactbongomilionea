import React from 'react';
import Header from "./Header";
import Footer from "./Footer";
//import '../styles/style.css';

class MainLayout extends React.Component {

  render() {
    const { children } = this.props;

    return (
        <>

          <div>

            <Header/>


            {children}

          </div>

          <Footer/>
        </>
    );
  }
}

export default MainLayout;
