import React from 'react';
import axios from "axios";
import { withRouter} from "react-router-dom";
import {Formik} from "formik";
import * as Yup from "yup";
import _ from "lodash";
import userImage from "../images/user.svg";
//import dompurify from 'dompurify';
//const parse = require('html-react-parser');

let regexPhone= /^[0-9]*$/;
const LoginSchema = Yup.object().shape({
  msisdn: Yup.string()
    .min(7, 'Minimum length is 7 characters')
    .max(15, 'Maximum length is 15 characters')
    .matches(regexPhone,'Invalid Format. Please begin with country code e.g 255...')
    .required('Phone number is Required')
});

class TestingPage extends React.PureComponent {

  constructor(props){
    super(props);
    this.state={
      isFetching: true,
      isSuccess: false,
      isError : false,
      token: '',
      addPhoneForm: {
        isVisible: true,
        isSubmitting: false,
        isSuccess: false,
        isError : false,
        msisdn:'',
        errorMessage:''
      },
      questionForm: {
        isVisible: false,
        isSubmitting: false,
        isSuccess: false,
        isError : false,
        answer1:'Send',
        answer2:'Cancel',
        errorMessage:''
      },
      topUpForm: {
        isVisible: false,
        isSubmitting: false,
        isSuccess: false,
        isError : false,
        question:'',
        amount:'',
        amountError:'',
        errorMessage:''
      },
      enterPinForm: {
        isVisible: false,
        isSubmitting: false,
        isSuccess: false,
        isError : false,
        question:'',
        errorMessage:'',
        answer1:'Send',
        answer2:'Cancel',
      },
      quitForm: {
        isVisible: false,
        isSubmitting: false,
        isSuccess: false,
        isError : false,
        question:'',
        errorMessage:''
      }
    }
    this.handlePhoneSubmit=this.handlePhoneSubmit.bind(this);
    this.handlePhoneReset=this.handlePhoneReset.bind(this);
    this.handleAnswerSubmit=this.handleAnswerSubmit.bind(this);
    this.handleTopUpChange=this.handleTopUpChange.bind(this);
    this.handleTopUpSubmit=this.handleTopUpSubmit.bind(this);
    this.handleTopUpReset=this.handleTopUpReset.bind(this);
    this.handlePinSubmit=this.handlePinSubmit.bind(this);
    this.handleQuitSubmit=this.handleQuitSubmit.bind(this);
    const CancelToken = axios.CancelToken;
    this.source = CancelToken.source();
  }

  componentDidMount() {

    console.log("fetching new token");
    axios.get(`/api/getToken`,{cancelToken: this.source.token})
      .then(response => {
        //console.log("this is the response");
        //console.log(response);
        if(response.data.token!=="") {
          console.log("this is the valid token "+response.data.token);
          this.setState({...this.state,token:response.data.token});
        }
      })
      .catch( (error)=> {
        // handle error
        //console.log(error);
        if (axios.isCancel(error)) {
          //console.log('Request canceled', error.message);
        } else {
          // handle error
        }
      })
      .then(function () {
        // always executed
      });
  }

  handlePhoneSubmit = (values) =>{
    const formData=new FormData();
    formData.append("msisdn",values.msisdn);
    formData.append("csrfToken",this.state.token);
    //console.log(values);
    this.submitPhone(formData);
  }

  submitPhone = (formData) => {
    this.setState({
      ...this.state,
      addPhoneForm: {
        ...this.state.addPhoneForm,
        isSubmitting:true,
        isSuccess: false,
        isError : false,
        msisdn:'',
        errorMessage:''
      }
    })
    axios({
      method: 'POST',
      data: formData,
      url:'/api/initiate',
      headers: {'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
    }).then(response =>{

      //console.log(response);
      //response.data = {"token":"d3cc2694b5106785a407c01457ab8d9adb819682-1609772132941-cbc438bc85105d5288c6a793","status":1,"displayNumber":1,"message":"Salio lako ni 100.00.\nOngeza salio kuendelea na Quiz, 200.00TZS kwa swali\n1.Ongeza\n2.Sitisha"}
      if(response.data.status===1){
        if(response.data.displayNumber === 1){
          /*const clean = dompurify.sanitize(response.data.message);
          const cleanNewsContent = parse(clean);
          console.log(cleanNewsContent);*/
          //console.log(response.data.answer1);
          //console.log(_.isEmpty(response.data.answer1));
          //console.log(response.data.answer2);
          //console.log(_.isEmpty(response.data.answer2));

          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              isSubmitting:false,
              isSuccess: true,
              isError : false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:true,
              isSubmitting:false,
              isSuccess: false,
              isError : false,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.questionForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.questionForm.answer2,
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 2){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.topUpForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.topUpForm.answer2,
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 3){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.enterPinForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.enterPinForm.answer2,
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 4){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:true,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:true,
              question : response.data.message,
              errorMessage:''
            }
          })
        }

      } else if(response.data.status!==1){
        this.setState({...this.state,pinErrorMsg:response.data.message});
      }
    }).catch( (error)=> {
      // handle error
      //console.log(error);
    }).then(function () {
      // always executed
    });
  }

  handlePhoneReset = (resetForm) => {
    resetForm();
  };

  handleAnswerSubmit = e =>{
    e.preventDefault();
    //console.log(e.target.value);
    const formData=new FormData();
    formData.append("answer",e.target.value);
    formData.append("csrfToken",this.state.token);
    this.submitAnswer(formData);
  }

  submitAnswer = (formData) => {
    this.setState({
      ...this.state,
      questionForm: {
        ...this.state.questionForm,
        isSubmitting:true,
        isSuccess: false,
        isError : false,
        errorMessage:''
      }
    })
    axios({
      method: 'POST',
      data: formData,
      url:'/api/getQuestions',
      headers: {'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
    }).then(response =>{

      //console.log(response);
      //response.data = {"token":"d3cc2694b5106785a407c01457ab8d9adb819682-1609772132941-cbc438bc85105d5288c6a793","status":1,"displayNumber":1,"message":"Salio lako ni 100.00.\nOngeza salio kuendelea na Quiz, 200.00TZS kwa swali\n1.Ongeza\n2.Sitisha"}
      if(response.data.status===1){
        if(response.data.displayNumber === 1){
          /*const clean = dompurify.sanitize(response.data.message);
          const cleanNewsContent = parse(clean);
          console.log(cleanNewsContent);*/
          //console.log(response.data.answer1);
          //console.log(_.isEmpty(response.data.answer1));
          //console.log(response.data.answer2);
          //console.log(_.isEmpty(response.data.answer2));

          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              isSubmitting:false,
              isSuccess: true,
              isError : false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:true,
              isSubmitting:false,
              isSuccess: false,
              isError : false,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.questionForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.questionForm.answer2,
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false
            }
          })
        }else if(response.data.displayNumber === 2){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.topUpForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.topUpForm.answer2,
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 3){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.enterPinForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.enterPinForm.answer2,
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 4){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:true,
              question : response.data.message,
              errorMessage:''
            }
          })
        }

      } else if(response.data.status!==1){
        this.setState({...this.state,pinErrorMsg:response.data.message});
      }
    }).catch( (error)=> {
      // handle error
      //console.log(error);
    }).then(function () {
      // always executed
    });
  }

  handleTopUpChange(e){
    const re = /^[0-9\b]+$/;
    //console.log(e.target.value);
    if (e.target.value === '' || re.test(e.target.value)) {
      this.setState({...this.state,topUpForm: {...this.state.topUpForm,amount:e.target.value}})
    }
  }

  handleTopUpSubmit = e =>{
    e.preventDefault();
    if(this.state.topUpForm.amount === '' || (Number.parseInt(this.state.topUpForm.amount) < 500 || Number.parseInt(this.state.topUpForm.amount) > 10000) ){
      this.setState({
        ...this.state,
        topUpForm: {
          ...this.state.topUpForm,
          amountError:'Amount should be between 500 and 10,000',
          errorMessage:''
        }
      })
      return false;
    }
    const formData=new FormData();
    formData.append("answer",this.state.topUpForm.amount);
    formData.append("csrfToken",this.state.token);
    //console.log("call topup api");
    this.submitTopUp(formData);
  }

  submitTopUp = (formData) => {
    this.setState({
      ...this.state,
      topUpForm: {
        ...this.state.topUpForm,
        isSubmitting:true,
        isSuccess: false,
        isError : false,
        errorMessage:''
      }
    })
    axios({
      method: 'POST',
      data: formData,
      url:'/api/getQuestions',
      headers: {'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
    }).then(response =>{

      //console.log(response);
      //response.data = {"token":"d3cc2694b5106785a407c01457ab8d9adb819682-1609772132941-cbc438bc85105d5288c6a793","status":1,"displayNumber":1,"message":"Salio lako ni 100.00.\nOngeza salio kuendelea na Quiz, 200.00TZS kwa swali\n1.Ongeza\n2.Sitisha"}
      if(response.data.status===1){
        if(response.data.displayNumber === 1){
          /*const clean = dompurify.sanitize(response.data.message);
          const cleanNewsContent = parse(clean);
          console.log(cleanNewsContent);*/
          //console.log(response.data.answer1);
          //console.log(_.isEmpty(response.data.answer1));
          //console.log(response.data.answer2);
          //console.log(_.isEmpty(response.data.answer2));

          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              isSubmitting:false,
              isSuccess: true,
              isError : false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:true,
              isSubmitting:false,
              isSuccess: false,
              isError : false,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.questionForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.questionForm.answer2,
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              isSubmitting:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              isSubmitting:false
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false
            }
          })
        }else if(response.data.displayNumber === 2){
          /*this.setState({
              ...this.state,
              token: response.data.token,
              addPhoneForm: {
                  ...this.state.addPhoneForm,
                  isVisible:false,
                  errorMessage:''
              },
              questionForm: {
                  ...this.state.questionForm,
                  isVisible:false,
                  question : '',
                  errorMessage:''
              },
              topUpForm: {
                  ...this.state.topUpForm,
                  isVisible:true,
                  question : response.data.message,
                  answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.topUpForm.answer1,
                  answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.topUpForm.answer2,
                  errorMessage:''
              },
              enterPinForm: {
                  ...this.state.enterPinForm,
                  isVisible:false,
                  question : '',
                  errorMessage:''
              },
              quitForm: {
                  ...this.state.quitForm,
                  isVisible:false,
                  question : '',
                  errorMessage:''
              }
          })*/
        }else if(response.data.displayNumber === 3){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.enterPinForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.enterPinForm.answer2,
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 4){
          /*this.setState({
              ...this.state,
              token: response.data.token,
              addPhoneForm: {
                  ...this.state.addPhoneForm,
                  isVisible:false,
                  errorMessage:''
              },
              questionForm: {
                  ...this.state.questionForm,
                  isVisible:false,
                  question : '',
                  errorMessage:''
              },
              topUpForm: {
                  ...this.state.topUpForm,
                  isVisible:true,
                  question : '',
                  errorMessage:''
              },
              enterPinForm: {
                  ...this.state.enterPinForm,
                  isVisible:false,
                  question : '',
                  errorMessage:''
              },
              quitForm: {
                  ...this.state.quitForm,
                  isVisible:true,
                  question : response.data.message,
                  errorMessage:''
              }
          })*/
        }

      } else if(response.data.status!==1){
        this.setState({...this.state,pinErrorMsg:response.data.message});
      }
    }).catch( (error)=> {
      // handle error
      //console.log(error);
    }).then(function () {
      // always executed
    });
  }

  handleTopUpReset(e){
    this.setState({
      ...this.state,
      topUpForm: {
        ...this.state.topUpForm,
        amount:'',
        amountError:'',
        errorMessage:''
      }
    })
  }

  handlePinSubmit = e =>{
    e.preventDefault();
    //console.log(e.target.value);
    const formData=new FormData();
    formData.append("answer",e.target.value);
    formData.append("csrfToken",this.state.token);
    this.submitPin(formData);
  }

  submitPin = (formData) => {
    this.setState({
      ...this.state,
      enterPinForm: {
        ...this.state.enterPinForm,
        isSubmitting:true,
        isSuccess: false,
        isError : false,
        errorMessage:''
      }
    })
    axios({
      method: 'POST',
      data: formData,
      url:'/api/getQuestions',
      headers: {'Content-Type': `multipart/form-data; boundary=${formData._boundary}` }
    }).then(response =>{

      //console.log(response);
      //response.data = {"token":"d3cc2694b5106785a407c01457ab8d9adb819682-1609772132941-cbc438bc85105d5288c6a793","status":1,"displayNumber":1,"message":"Salio lako ni 100.00.\nOngeza salio kuendelea na Quiz, 200.00TZS kwa swali\n1.Ongeza\n2.Sitisha"}
      if(response.data.status===1){
        if(response.data.displayNumber === 1){
          /*const clean = dompurify.sanitize(response.data.message);
          const cleanNewsContent = parse(clean);
          console.log(cleanNewsContent);*/
          //console.log(response.data.answer1);
          //console.log(_.isEmpty(response.data.answer1));
          //console.log(response.data.answer2);
          //console.log(_.isEmpty(response.data.answer2));

          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:true,
              isSubmitting:false,
              isSuccess: false,
              isError : false,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.questionForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.questionForm.answer2,
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              isSubmitting:false
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false
            }
          })
        }else if(response.data.displayNumber === 2){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.topUpForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.topUpForm.answer2,
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 3){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:true,
              question : response.data.message,
              answer1: !_.isEmpty(response.data.answer1) ? response.data.answer1 : this.state.enterPinForm.answer1,
              answer2: !_.isEmpty(response.data.answer2) ? response.data.answer2 : this.state.enterPinForm.answer2,
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:false,
              question : '',
              errorMessage:''
            }
          })
        }else if(response.data.displayNumber === 4){
          this.setState({
            ...this.state,
            token: response.data.token,
            addPhoneForm: {
              ...this.state.addPhoneForm,
              isVisible:false,
              errorMessage:''
            },
            questionForm: {
              ...this.state.questionForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            topUpForm: {
              ...this.state.topUpForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            enterPinForm: {
              ...this.state.enterPinForm,
              isVisible:false,
              question : '',
              errorMessage:''
            },
            quitForm: {
              ...this.state.quitForm,
              isVisible:true,
              question : response.data.message,
              errorMessage:''
            }
          })
        }

      } else if(response.data.status!==1){
        this.setState({...this.state,pinErrorMsg:response.data.message});
      }
    }).catch( (error)=> {
      // handle error
      //console.log(error);
    }).then(function () {
      // always executed
    });
  }

  handleQuitSubmit = e =>{
    e.preventDefault();
    //console.log(e.target.value);
    this.setState({
      ...this.state,
      addPhoneForm: {
        ...this.state.addPhoneForm,
        isVisible:true,
        errorMessage:''
      },
      questionForm: {
        ...this.state.questionForm,
        isVisible:false,
        question : '',
        errorMessage:''
      },
      topUpForm: {
        ...this.state.topUpForm,
        isVisible:false,
        question : '',
        errorMessage:''
      },
      enterPinForm: {
        ...this.state.enterPinForm,
        isVisible:false,
        question : '',
        errorMessage:''
      },
      quitForm: {
        ...this.state.quitForm,
        isVisible:false,
        question : '',
        errorMessage:''
      }
    })
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
  }

  render() {

    return (
      <main className="login-form">
        <div className="cotainer">
          <div className="row justify-content-center">
            <div className="col-md-7">
              <div className="account-wall">
                  <div className="text-center image" align="center">
                    <img src={userImage} alt="user" title="user" className="img-responsive outter"/>
                  </div>
                  {this.state.addPhoneForm.isVisible &&
                  <Formik
                    initialValues={{
                      msisdn: ''
                    }}
                    validationSchema={LoginSchema}
                    onSubmit={this.handlePhoneSubmit}
                    onChange={(values) => {
                      //console.log(values);
                    }}

                  >
                    {({handleSubmit, handleBlur, handleChange, errors, touched, values, resetForm}) => (

                      <form onSubmit={handleSubmit} autoComplete="off">
                        <input type="hidden" name="csrfToken" id="csrfToken" value={this.state.token}/>
                        <br/>
                        <div className="text-center text-gray">
                          <h2 className="weight-500 mb-1 phoneNumber">
                            <b>Karibu Simba Bingwa</b>
                            <br/>
                            Jibu Maswali ushinde hadi Milioni 10 kila siku
                          </h2>
                          <br/>
                          <p className="h4 mb-2 weight-300 questions">
                            <b>Weka namba yako ya simu</b>
                            <br/>
                          </p>
                          <br/>
                        </div>
                        <div className="row">
                          <div className="col-sm-6 offset-md-3">
                            <div className="form-group">
                              <div className="input-group">
                                <input type="tel" id="msisdn" name="msisdn" className=
                                  {errors.msisdn && touched.msisdn
                                    ? "form-control text-center msisdn text-input second emc error"
                                    : "form-control text-center msisdn text-input second emc"
                                  }
                                       placeholder="Format(255754XXXXXX)"
                                       onBlur={handleBlur}
                                       onChange={handleChange}
                                       value={values.msisdn}
                                />
                              </div>
                              <label id="msisdnError"
                                     className="error">{errors.msisdn && touched.msisdn ? (errors.msisdn) : ''}</label>
                            </div>
                          </div>
                        </div>

                        <div className="text-center">
                          <div className="form-group row">
                            <div className="col-md-6 offset-md-3">
                              <input type="submit" className="btn btn-success rpt" value="send"/>&nbsp;
                              <input type="button" className="btn btn-default cancel rpt"
                                     value="Cancel" onClick={this.handlePhoneReset.bind(null, resetForm)}/>
                            </div>
                          </div>
                        </div>
                      </form>
                    )}
                  </Formik>
                  }
                  {this.state.questionForm.isVisible &&
                  <form id="questionForm">
                    <br/> <br/> <br/>
                    <div className="text-center text-gray">
                      <p className="h4 mb-2 weight-300 questions" style={{whiteSpace:'pre-wrap'}}>
                        {this.state.questionForm.question}
                      </p>
                      <br/>
                    </div>
                    <div className="text-center">
                      <div className="form-group row">
                        <div className="col-md-6 offset-md-3">
                          <div className="form-group">
                            <button type="button" className="btn btn-success rpt nextQuestion"
                                    value="1" onClick={this.handleAnswerSubmit}>
                              {this.state.questionForm.answer1}
                            </button>&nbsp;
                            <button type="button" className="btn btn-default cancel rpt nextQuestion"
                                    value="2" onClick={this.handleAnswerSubmit}>
                              {this.state.questionForm.answer2}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  }
                  {this.state.topUpForm.isVisible &&
                  <form id="topUpForm" onSubmit={this.handleTopUpSubmit}>
                    <br/>
                    <div className="text-center text-gray">
                      <h2 className="weight-500 mb-1 phoneNumber">
                        <b>{this.state.topUpForm.question}</b>
                      </h2>
                      <br/>
                    </div>
                    <div className="row">
                      <div className="col-sm-6 offset-md-3">
                        <div className="form-group">
                          <div className="input-group">
                            <input type="text" name="amount"
                                   className="form-control text-center amount"
                                   onChange={this.handleTopUpChange}
                                   value={this.state.topUpForm.amount}
                            />
                          </div>
                          <label>{this.state.topUpForm.amountError}</label>
                        </div>
                      </div>
                    </div>
                    <div className="text-center">
                      <div className="form-group row">
                        <div className="col-md-6 offset-md-3">
                          <input type="submit" className="btn btn-success rpt" value="Send"/>&nbsp;
                          <input type="button" className="btn btn-default cancel rpt"
                                 value="Cancel" onClick={this.handleTopUpReset}/>
                        </div>
                      </div>
                    </div>
                  </form>
                  }
                  {this.state.enterPinForm.isVisible &&
                  <form id="enterPinForm">
                    <br/> <br/> <br/>
                    <div className="text-center text-gray">
                      <p className="h4 mb-2 weight-300 questions">
                        <span id="pinQuestion">{this.state.enterPinForm.question}</span>
                      </p>
                      <br/>
                    </div>
                    <div className="text-center">
                      <div className="form-group row">
                        <div className="col-md-6 offset-md-3">
                          <div className="form-group">
                            <button type="button" className="btn btn-success rpt nextQuestion"
                                    value="1" onClick={this.handlePinSubmit}>
                              {this.state.enterPinForm.answer1}
                            </button>&nbsp;
                            <button type="button" className="btn btn-default cancel rpt nextQuestion"
                                    value="2" onClick={this.handlePinSubmit}>
                              {this.state.enterPinForm.answer2}
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  }
                  {this.state.quitForm.isVisible &&
                  <form id="quitForm">
                    <br/> <br/> <br/>
                    <div className="text-center text-gray">
                      <p className="h4 mb-2 weight-300 questions">
                        <span id="quitQuestion">{this.state.quitForm.question}</span>
                      </p>
                      <br/>
                    </div>
                    <div className="text-center">
                      <div className="form-group row">
                        <div className="col-md-6 offset-md-3">
                          <div className="form-group">
                            <button className="btn btn-success rpt" onClick={this.handleQuitSubmit}>
                              Mwanzo
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  }
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

/*const mapDispatchToProps= (dispatch) => ({
    setIsTimedOut : () => dispatch(setIsTimedOut()),
    setIsLoggedInButNotSubbed : () => dispatch(setIsLoggedInButNotSubbed()),
    fetchNormMusicSuccess : (entities,result) => dispatch(fetchNormMusicSuccess(entities,result)),
    fetchNormVideosSuccess : (entities,result) => dispatch(fetchNormVideosSuccess(entities,result)),
    fetchNormPicturesSuccess : (entities,result) => dispatch(fetchNormPicturesSuccess(entities,result)),
    fetchNormNewsSuccess : (entities) => dispatch(fetchNormNewsSuccess(entities)),
    fetchNormVideoCategoriesSuccess : (entities,result) => dispatch(fetchNormVideoCategoriesSuccess(entities,result)),
    fetchNormArtistsSuccess : (entities,result) => dispatch(fetchNormArtistsSuccess(entities,result)),
})

const makeMapState = () => {

    const mapStateToProps = (state) => {

        return {
            normMusicListName : activeListName(state),
            activeMusicIndex : activeMusicIndex(state),
            isMusicPlaying : isMusicPlaying(state),
        }
    };

    return mapStateToProps
};*/

export default (withRouter(TestingPage));
