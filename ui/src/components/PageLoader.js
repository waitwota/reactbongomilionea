import React from 'react';
import Loader from "react-loader-spinner";

const PageLoader = () => {
  return (
    <div style={{
      width: "100%",
      height: "100%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }}>
      <div style={{maxHeight: "100%",paddingTop:"20%",paddingLeft:"-9%"}}>
        <Loader type="Oval" color="#ff0000" height={100} width={100} />
      </div>
    </div>
  );
};

export default PageLoader;
