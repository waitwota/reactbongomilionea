package repository;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.util.TextUtils;
import play.filters.csrf.CSRF;
import play.mvc.Http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class API {

    public static String getMonthSeconds(){

        return "2592000";
    }

    public static long getCurrentStamp(){

        return System.currentTimeMillis()/1000;
    }

    public static Date getCurrentDate(){
        LocalDateTime newLocalDateTime=LocalDateTime.now();
        return  Date.from(newLocalDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date getNow(){
        return  Date.from(Instant.now());
    }

    public static Date getToday(){
        LocalDate today =LocalDate.now();
        return  API.getDateFromLocalDateStartofDay(today);
    }

    public static LocalDate getTodayLocalDate(){
        return  LocalDate.now();
    }

    public static long getStampFromDate(Date date){

        return date.getTime() / 1000;
    }

    public static Date getDateFromStamp(long timestamp){

        LocalDateTime newLocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
                TimeZone.getDefault().toZoneId());

        return Date.from(newLocalDateTime.atZone(ZoneId.systemDefault()).toInstant());

    }

    public static LocalDateTime getLocalDateTimeFromStamp(long timestamp){

        return LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
                TimeZone.getDefault().toZoneId());

    }

    public static LocalDate getLocalFromDate(Date date){

        Instant instant = date.toInstant();
        return  instant.atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDateTime getLocalDateTimeFromDate(Date date){

        Instant instant = date.toInstant();
        return  instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date getDateFromLocalDateStartofDay(LocalDate newLocalDate){
        return  Date.from(newLocalDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getDateFromLocalDateTime(LocalDateTime newLocalDateTime){
        return Date.from(newLocalDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static String getDateddmmyyyy(long timestamp){

        LocalDateTime localDateTime= API.getLocalDateTimeFromStamp(timestamp);
        return DateTimeFormatter.ofPattern("E dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(localDateTime);
    }

    public static String getDateSlashyyyyMMddHHmm(Date date){
        LocalDateTime localDateTime= API.getLocalDateTimeFromDate(date);
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm", Locale.ENGLISH).format(localDateTime);
    }

    public static String getDateSlashyyyyMMddHHmm(){
        ZonedDateTime defaultDateTime = ZonedDateTime.now(ZoneId.systemDefault());
        return DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm", Locale.ENGLISH).format(defaultDateTime);
    }

    public static String getDateSlashddMMyyyyHHmmss(LocalDateTime localDateTime){
        return DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss", Locale.ENGLISH).format(localDateTime);
    }

    public static String getDateHyphenyyyyMMddHHmmss(long timestamp){

        LocalDateTime localDateTime= API.getLocalDateTimeFromStamp(timestamp);
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(localDateTime);
    }

    public static String getDateHyphenyyyyMMddHHmmss(LocalDateTime dateTime){

        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(dateTime);
    }

    public static String getTimeAloneFromStamp(long timestamp){

        LocalDateTime localDateTime= API.getLocalDateTimeFromStamp(timestamp);
        return DateTimeFormatter.ofPattern("HH:mm", Locale.ENGLISH).format(localDateTime);
    }

    public static Date getBeginOfMonth(){
        LocalDate today = LocalDate.now();
        return API.getDateFromLocalDateStartofDay(today.withDayOfMonth(1));
    }

    public static Date getEndOfMonth(){
        LocalDate today = LocalDate.now();
        return API.getDateFromLocalDateStartofDay(today.plusMonths(1).withDayOfMonth(1).minusDays(1));
    }

    public static Date getBeginOfNextMonth(){
        LocalDate today = LocalDate.now();
        return API.getDateFromLocalDateStartofDay(today.plusMonths(1).withDayOfMonth(1));
    }

    public static String getDayofWeek(LocalDate localDate){

        return localDate.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.US);
    }

    public static int getDay(LocalDate localDate){

        return localDate.getDayOfMonth();
    }

    public static String getMonth(LocalDate localDate){

        return localDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.US);
    }

    public static LocalDateTime getTodayMidnight(){
        return LocalDateTime.of(API.getTodayLocalDate(), LocalTime.MIDNIGHT);
    }

    public static LocalDateTime getYesterdayMidnight(){
        return LocalDateTime.of(API.getTodayLocalDate().minusDays(1), LocalTime.MIDNIGHT);
    }

    public static LocalDateTime getDaysBefore(int daysBefore){
        return LocalDateTime.of(API.getTodayLocalDate().minusDays(daysBefore),LocalTime.now());
    }

    public static LocalDateTime getTomorrowMidnight(){
        return LocalDateTime.of(API.getTodayLocalDate().plusDays(1), LocalTime.MIDNIGHT);
    }

    public static String getGMT(Date date){
        //Instant instant = date.toInstant();
        //LocalDateTime gmtDatetime=LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        OffsetDateTime zdtAtAsia = LocalDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC).atOffset(ZoneOffset.UTC);
        return  DateTimeFormatter.RFC_1123_DATE_TIME.format(zdtAtAsia);
    }

    public static LocalDateTime getLocalDateFromStamp(long timestamp){

        return LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
                TimeZone.getDefault().toZoneId());

    }

    public static LocalDateTime getLocalDateFromDate(Date date){

        Instant instant = date.toInstant();
        return  instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static long getStampMinsFromNow(int toMinutes){

        return getCurrentStamp()+(toMinutes*60);
    }

    public static long getElapsedTime(long timestamp){

        return getCurrentStamp()-timestamp;
    }

    public static Date getDateFromStamp(Long timestamp){
        Date date=new Date();
        return date;
    }

    public static String getElapsedString(long stamp,boolean isStamp){
        long seconds=stamp;
        if(isStamp){
            seconds =getElapsedTime(stamp);
        }
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
        String dayPart,hrsPart,minPart,secPart,elapsedTime="";
        if(day>0){
            if(day<3){
                //dayPart=day+" days";
                dayPart=day+"d";
            }else{

                return API.getDateddmmyyyy(stamp);
            }
            elapsedTime+=dayPart+" ";
        }
        if(hours>0){
            if(hours>1){
                hrsPart=hours+"h";
            }else{
                hrsPart=hours+"h";
            }
            elapsedTime+=hrsPart+" ";
        }
        if(minute>0){
            if(minute>1){
                minPart=minute+"m";
            }else{
                minPart=minute+"m";
            }
            elapsedTime+=minPart+" ";
        }
        if(second>0){
            if(second>1){
                secPart=second+"s";
            }else{
                secPart=second+"s";
            }
            elapsedTime+=secPart;
        }
        return elapsedTime+" ago";

    }

    public static String getElapsedStringShort(long stamp,boolean isStamp){
        long seconds=stamp;
        if(isStamp){
            seconds =getElapsedTime(stamp);
        }
        int day = (int) TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds)* 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) *60);
        LocalDateTime todayMidnight = API.getTodayMidnight();
        LocalDateTime yesterdayMidnight = API.getYesterdayMidnight();
        LocalDateTime thisLocalDate= API.getLocalDateTimeFromStamp(stamp);
        if(thisLocalDate.isBefore(todayMidnight)&&thisLocalDate.isAfter(yesterdayMidnight)){
            return "Yesterday at "+ API.getTimeAloneFromStamp(stamp);
        }
        if(day>0){
            if(day==1){
                return day+" day ago";
            }else if(day<7){
                return day+" days ago";
            }else if(day<30){
                int week=day/7;
                if(week>1){
                    return week+" weeks ago";
                }else{
                    return week+" week ago";
                }
            }else if(day<365){
                int month=day/30;
                if(month>1){
                    return month+" months ago";
                }else{
                    return month+" month ago";
                }
            }else{
                int year=day/365;
                if(year>1){
                    return year+" years ago";
                }else{
                    return year+" year ago";
                }
            }
        }
        if(hours>0){
            if(hours>1){
                return hours+" hours ago";
            }else{
                return hours+" hour ago";
            }
        }
        if(minute>0){
            if(minute>1){
                return minute+" minutes ago";
            }else{
                return minute+" minute ago";
            }
        }
        if(second>0){
            if(second<10){
                return "just now";
            }else{
                return second+" seconds ago";
            }
        }
        return "just now";
    }

    //
    public static long pl(String newString){

        return Long.parseLong(newString);

    }

    public static int pi(String newString){

        return Integer.parseInt(newString);

    }

    public static double pd(String newString){

        return Double.parseDouble(newString);

    }

    public static String generateRandom(){

        return new SecureRandom().nextInt(900000) + 100000+"";

    }

    public static String generateRandom(String beginChar){

        return beginChar+ API.generateRandom();

    }

    public static boolean isLoggedIn(Http.Request request){
        return request.session().get("msisdn").isPresent() &&
                request.session().get("sessionid").isPresent();
    }

    public static Long getThisUserId(Http.Request request){
        if(request.session().get("connected").isPresent()){
            return API.pl(request.session().get("connected").get());
        }else{
            return null;
        }
    }

    public static boolean isAdmin(Http.Request request){
        if(request.session().get("isAdmin").isPresent() &&
                request.session().get("isAdmin").get().equals("1")){
            return true;
        }else{
            return false;
        }
    }

    public static boolean isArtist(Http.Request request){
        if(request.session().get("isArtist").isPresent() &&
                request.session().get("isArtist").get().equals("1")){
            return true;
        }else{
            return false;
        }
    }

    public static boolean isVerified(Http.Request request){
        if(request.session().get("isVerified").isPresent() &&
                request.session().get("isVerified").get().equals("1")){
            return true;
        }else{
            return false;
        }
    }

    public static String getPlayerId(Http.Request request){
        if(isLoggedIn(request) && API.ne(request.session().get("player_id").get())){
            return request.session().get("player_id").get();
        }else{
            return "0";
        }
    }

    public static String getMsisdn(Http.Request request){
        if(isLoggedIn(request)){
            return request.session().get("msisdn").get();
        }else{
            return null;
        }
    }

    public static String getSessionId(Http.Request request){
        if(isLoggedIn(request)){
            return request.session().get("sessionid").get();
        }else{
            return null;
        }
    }


    public static String getBalance(Http.Request request){
        if(isLoggedIn(request) && API.ne(request.session().get("balance").get())){
            return request.session().get("balance").get();
        }else{
            return "0";
        }
    }

    public static String getPartnerName(Http.Request request){
        if(isLoggedIn(request) && API.ne(request.session().get("partner_name").get())){
            return request.session().get("partner_name").get();
        }
        return "Mnada Poa";
    }

    public static String getUserRole(Http.Request request){

        if(isLoggedIn(request)){

            return request.session().get("userRole").get();

        }else{
            return null;
        }
    }

    public static boolean hasEnteredPhone(Http.Request request){

        if(request.session().get("phoneNumber").isPresent()){
            return true;
        }else{
            return false;
        }
    }

    public static String getEnteredphone(Http.Request request){

        if(hasEnteredPhone(request)){
            return request.session().get("phoneNumber").get();
        }else{
            return null;
        }
    }

    public static String cleanString(String checkString){

        if(TextUtils.isEmpty(checkString)) {
            return "";
        }else{
            return checkString;
        }
    }

    public static String getRandomString(){
        return new SecureRandom().ints(0,36)
                .mapToObj(i -> Integer.toString(i, 36))
                .map(String::toUpperCase).distinct().limit(8).collect(Collectors.joining())
                .replaceAll("([A-Z0-9]{4})", "$1-").substring(0,9);
    }

    public static String getRandomStringStamp(){

        return new SecureRandom().ints(0,36)
                .mapToObj(i -> Integer.toString(i, 36))
                .map(String::toUpperCase).distinct().limit(8).collect(Collectors.joining())
                .replaceAll("([A-Z0-9]{4})", "$1-").substring(0,9)+"_"+ API.getCurrentStamp();
    }

    public static String getRandomVideoName(){

        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( 12 );
        for( int i = 0; i < 12; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return "VF_"+sb.toString();
    }

    public static String getRandomVideoThumbName(){

        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( 12 );
        for( int i = 0; i < 12; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return "VT_"+sb.toString();
    }

    public static boolean isTimeoutStillValid(Http.Request request, int sessionTimeout,Long currentStamp){

        if(isLoggedIn(request))
        {
            //System.out.println("Compare time for timeout");
            return isValidTimeOut(request, sessionTimeout,currentStamp);
        }else{
            return false;
        }
    }

    public static boolean isSubscriptionStillValid(Http.Request request, int sessionTimeout,Long currentStamp){

        if(request.session().get("subStartStamp").isPresent()
                && !request.session().get("subStartStamp").get().equals("")
                &&  request.session().get("subEndStamp").isPresent()
                && !request.session().get("subEndStamp").get().equals(""))
        {
            Long subStartStamp = API.pl(request.session().get("subStartStamp").get());
            Long subEndStamp = API.pl(request.session().get("subEndStamp").get());
            if(subStartStamp <= currentStamp && subEndStamp > currentStamp){
                return true;
            }
            return false;
        }else{
            return false;
        }
    }

    public static boolean isValidSessionAdminArtist(Http.Request request,int sessionTimeout, Long currentStamp){

        if(request.session().get("connected").isPresent()
                && !request.session().get("connected").get().equals("")
                && (
                (request.session().get("isAdmin").isPresent() && request.session().get("isAdmin").get().equals("1"))
                        ||
                        (request.session().get("isArtist").isPresent() && request.session().get("isArtist").get().equals("1"))
        )
        ){
            return isValidTimeOut(request, sessionTimeout, currentStamp);
        }else{
            return false;
        }
    }

    public static boolean isArtistorAdmin(Http.Request request){

        return (request.session().get("isAdmin").isPresent() && request.session().get("isAdmin").get().equals("1"))
                || (request.session().get("isArtist").isPresent() && request.session().get("isArtist").get().equals("1"));
    }

    public static boolean isVerifiedArtistorAdmin(Http.Request request){

        return (request.session().get("isAdmin").isPresent() && request.session().get("isAdmin").get().equals("1"))
                || (
                (request.session().get("isArtist").isPresent() && request.session().get("isArtist").get().equals("1"))
                        &&
                        (request.session().get("isVerified").isPresent() && request.session().get("isVerified").get().equals("1"))
        );
    }

    private static boolean isValidTimeOut(Http.Request request, int sessionTimeout, Long currentStamp) {

        if(request.session().get("latestStamp").isPresent()
                && !request.session().get("latestStamp").get().equals(""))
        {
            Long latestamp = API.pl(request.session().get("latestStamp").get());
            int timeDifference = (currentStamp.intValue() - latestamp.intValue());
            //System.out.println("Latest stamp is: "+latestamp);
            //System.out.println("Current stamp is: "+currentStamp);
            //System.out.println("Difference is :" + timeDifference + " seconds which is : " + TimeUnit.SECONDS.toMinutes(timeDifference) + " minutes");
            if (timeDifference > sessionTimeout) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidSession(Http.Request request,int sessionTimeout){

        if(request.session().get("connected").isPresent()
                && !request.session().get("connected").get().equals("")
                && request.session().get("latestStamp").isPresent()
                && !request.session().get("latestStamp").get().equals("")){
            Long latestamp= API.pl(request.session().get("latestStamp").get());
            Long currentStamp= API.getCurrentStamp();
            int timeDifference=(currentStamp.intValue()-latestamp.intValue());
            //System.out.println("Latest stamp is: "+latestamp);
            //System.out.println("Current stamp is: "+currentStamp);
            //System.out.println("Differnce is :" +timeDifference+" which is :"+TimeUnit.SECONDS.toMinutes(timeDifference));
            if(timeDifference>sessionTimeout){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    public static String getRandomReference(int length){

        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public static String getNewRandomReference(){

        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder( 6 );
        for( int i = 0; i < 6; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString().toUpperCase()+ API.getCurrentStamp();
    }

    public static String getCompanyRef(){

        return new SecureRandom().ints(0,36)
                .mapToObj(i -> Integer.toString(i, 36))
                .map(String::toUpperCase).distinct().limit(8).collect(Collectors.joining())
                .replaceAll("([A-Z0-9]{4})", "$1-").substring(0,9)+"-"+ API.getCurrentStamp();
    }

    public static String getEncodedString(String stringToEncode){

        try {
            return URLEncoder.encode(stringToEncode,"UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean ne(String checkString) {
        return StringUtils.isNotBlank(checkString);
    }

    public static String getCSRFToken(Http.Request request){
        return CSRF.getToken(request).map(CSRF.Token::value).orElse("no token");
    }

    public static List<JsonNode> sortJsonNode(JsonNode rootResponse, String sortKey) {
        List<JsonNode> list = new ArrayList<>();
        if(rootResponse != null){
            Iterator<JsonNode> i =rootResponse.elements();
            while(i.hasNext()){
                list.add(i.next());
            }
            Comparator<JsonNode> comparator = Comparator.comparingInt(o -> o.get(sortKey).asInt());
            list.sort(comparator.reversed());
        }
        return list;
    }

    public static List<ObjectNode> processJsonNodeToObjectNode(JsonNode rootResponse, String sortKey) {
        List<ObjectNode> list = new ArrayList<>();
        List<DateTimeFormatter> dateTimeFormatters = API.getDateTimeFormatters();
        try{
            if(rootResponse != null){
                ArrayNode objectNode =  (ArrayNode) rootResponse;
                Iterator<JsonNode> i =objectNode.elements();
                while(i.hasNext()){
                    ObjectNode node = (ObjectNode) i.next();
                    node.put("compare_date",0L);
                    if(node.has(sortKey)){
                        LocalDateTime localDateTime = API.parseDateFromString(dateTimeFormatters,node.get(sortKey).asText(""));
                        if(localDateTime != null){
                            node.put("compare_date", API.getStampFromDate(API.getDateFromLocalDateTime(localDateTime)));
                            node.put("new_closure_date", API.getDateSlashddMMyyyyHHmmss(localDateTime));
                        }
                    }
                    if(node.has("mime_type")){
                        node.put("mime_type",StringUtils.substringAfterLast(node.get("mime_type").asText(),"/"));
                    }
                    if(node.has("mimetype")){
                        node.put("mimetype",StringUtils.substringAfterLast(node.get("mimetype").asText(),"/"));
                    }
                    if(node.has("player")){
                        node.put("player",StringUtils.substring(node.get("player").asText(),0,9)+"XXX");
                    }
                    if(node.has("bid_amount")){
                        node.put("bid_amount",StringUtils.substringBeforeLast(node.get("bid_amount").asText(),"."));
                    }
                    list.add(node);
                }
            }
        }catch (Exception ignored){

        }
        return list;
    }

    public static void sortObjectNode(List<ObjectNode> list) {
        try {
            if (list != null && !list.isEmpty()) {
                Comparator<ObjectNode> comparator = Comparator.comparingLong(o -> o.get("compare_date").asLong());
                list.sort(comparator.reversed());
            }
        }catch (Exception ignored){

        }
    }

    public static LinkedHashMap<String,Integer> getCodeToNumCountMap(JsonNode rootResponse) {
        LinkedHashMap<String,Integer> codeNumCountMap = new LinkedHashMap<>();
        try {
            if (rootResponse != null) {
                Iterator<JsonNode> i = rootResponse.elements();
                while (i.hasNext()) {
                    String winningCode = i.next().get("winning_code").asText();
                    if (!codeNumCountMap.containsKey(winningCode)) {
                        int count = 1;
                        if(winningCode.equalsIgnoreCase("156")){
                            count +=5;
                        }
                        codeNumCountMap.put(winningCode, count);
                    } else {

                        int count = codeNumCountMap.get(winningCode);
                        count++;

                        codeNumCountMap.put(winningCode, count);
                    }
                }
            }
        }catch (Exception ignored){

        }
        return codeNumCountMap;
    }

    public static LinkedHashMap<String, Integer> sortMapByValues(LinkedHashMap<String, Integer> codeNumCountMap,boolean isAscending) {
        try {
            Comparator<Map.Entry<String,Integer>> comparator;
            if(isAscending){
                comparator = Map.Entry.comparingByValue();
            }else{
                comparator = Map.Entry.comparingByValue(Comparator.reverseOrder());
            }
            codeNumCountMap = codeNumCountMap.entrySet().stream()
                    .sorted(comparator)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        }catch (Exception ignored){

        }
        return codeNumCountMap;
    }

    public static LocalDateTime parseDateFromString(List<DateTimeFormatter> dateTimeFormatters,String thisDate){

        for (DateTimeFormatter formatter : dateTimeFormatters) {
            try {
                return LocalDateTime.parse(thisDate, formatter);
            } catch (Exception pe) {
                // Loop on
            }
        }
        return null;
    }

    public static List<DateTimeFormatter> getDateTimeFormatters() {
        List<DateTimeFormatter> dateTimeFormatters = new ArrayList<DateTimeFormatter>();
        dateTimeFormatters.add(DateTimeFormatter.ofPattern("MMM d, yyyy h:m:s a", Locale.ENGLISH));
        dateTimeFormatters.add(DateTimeFormatter.ofPattern("MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH));
        dateTimeFormatters.add(DateTimeFormatter.ofPattern("MMM dd, yyyy h:mm:ss a", Locale.ENGLISH));
        return dateTimeFormatters;
    }

    public static String buildJsonNodeFromObject(Object object) {

        String message = null;

        try {

            GsonBuilder gb = new GsonBuilder();
            gb.serializeNulls();
            Gson gson = gb.create();
            message = gson.toJson(object);

        } catch (Exception e) {
            System.out.println("Error Found in Building Message Payload: "+ e);
        }

        return message;

    }

}
