package repository.dto;
import org.apache.commons.lang3.StringUtils;

public class LoginObject {

    private String msisdn;
    private String otp;
    private String answer;
    private String partner_name;
    private int topupAmount = 0;

    public LoginObject() {
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public int getTopupAmount() {
        return topupAmount;
    }

    public void setTopupAmount(int topupAmount) {
        this.topupAmount = topupAmount;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isValidLoginObject(){
        return (StringUtils.isNotBlank(this.otp) && StringUtils.isNotBlank(this.partner_name) && StringUtils.isNotBlank(this.msisdn));
    }

    public boolean isValidLoginInitiationObject(){
        return (StringUtils.isNotBlank(this.msisdn));
    }

    public boolean isValidLoginQuestionsObject(){
        return (StringUtils.isNotBlank(this.answer));
    }

    public boolean isValidPlayerValidationObject(){
        return (StringUtils.isNotBlank(this.partner_name) && StringUtils.isNotBlank(this.msisdn));
    }

    public boolean isValidRegisterTopUpObject(){
        return (topupAmount >= 500 && topupAmount <= 200000);
    }
}

