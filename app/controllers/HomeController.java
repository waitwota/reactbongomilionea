package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import play.Environment;
import play.data.Form;
import play.data.FormFactory;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.CSRF;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.*;
import repository.API;
import repository.dto.AnswerObject;
import repository.dto.LoginObject;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

public class HomeController extends Controller {

    private final HttpExecutionContext httpExecutionContext;
    private final FormFactory formFactory;
    private final Environment environment;
    private final Config config;
    private final WSClient ws;
    private static String newQuery="";
    private static String SERVER_HOST;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(HomeController.class);

    private Form<LoginObject> loginRequestForm;
    private final static String INITIATE_URL = "http://survey-gw.fasthub.co.tz/survey/ussd/receiver";
    private final static String KEY_WORD = "149262";
    private final static String COUNTRY = "Tanzania";
    private final static String OPERATOR = "web";
    private final static int IS_SHOW_NORMAL = 1;
    private final static int IS_SHOW_ENTER_AMOUNT = 2;
    private final static int IS_SHOW_ENTER_PIN = 3;
    private final static int IS_SHOW_QUIT_FORM = 4;

    @Inject
    public HomeController(HttpExecutionContext httpExecutionContext,
                          FormFactory formFactory,
                          Environment environment,
                          Config config,
                          WSClient ws
    ) {
        this.httpExecutionContext = httpExecutionContext;
        this.config = config;
        this.environment=environment;
        this.formFactory=formFactory;
        this.loginRequestForm = this.formFactory.form(LoginObject.class);
        this.ws=ws;
        SERVER_HOST=config.getString("serverHost");
    }

    @AddCSRFToken
    public CompletionStage<Result> getToken(Http.Request request) {
        ObjectNode result = Json.newObject();
        String token= CSRF.getToken(request).map(CSRF.Token::value).orElse("no token");
        result.put("token",token);
        logger.info("This is the CSRF token "+token);
        return CompletableFuture.completedFuture(ok(result));
    }

    public CompletionStage<Result> logout(Http.Request request) {
        logger.info("Session before deletion: "+request.session().toString());
        ObjectNode result=Json.newObject();
        result.put("status",440);
        return CompletableFuture.completedFuture(unauthorized(result)
                .withNewSession().discardingCookie("PLAY_SESSION"));
    }

    public CompletionStage<Result> initiate(Http.Request request) {

        ObjectNode result = Json.newObject();
        String token =API.getCSRFToken(request);
        result.put("token",token);
        Form<LoginObject> loginObjectForm = loginRequestForm.bindFromRequest(request);
        LoginObject loginObject = loginObjectForm.get();
        if(loginObject != null && loginObject.isValidLoginInitiationObject()){
            WSRequest wsRequest = ws.url(INITIATE_URL);
            String sessionId = API.getCurrentStamp()+"";
            CompletionStage<? extends WSResponse> wsResponse =wsRequest
                    .addQueryParameter("input",KEY_WORD)
                    .addQueryParameter("sessionid",sessionId)
                    .addQueryParameter("msisdn",loginObject.getMsisdn())
                    .addQueryParameter("country",COUNTRY)
                    .addQueryParameter("operator",OPERATOR)
                    .get();

            Function<WSResponse, Result> onSuccess = response -> {
                logger.info(response.getBody());

                if (response.getStatus() != 200) {
                    result.put("status",0);
                    result.put("message","Wrong phone number or password");
                    return ok(result);
                }else{
                    String responseBody = "Tafadhari weka kiasi:";
                    responseBody = response.getBody();
                    int displayNumber =getDisplayNumber(responseBody);
                    AnswerObject answerObject = getAnswers(responseBody);
                    result.put("status",1);
                    result.put("displayNumber",displayNumber);
                    result.put("message",responseBody);
                    result.put("answer1",answerObject.getAnswer1());
                    result.put("answer2",answerObject.getAnswer2());
                    return ok(result)
                            .withNewSession()
                            .addingToSession(request,"csrfToken",token)
                            .addingToSession(request,"msisdn",loginObject.getMsisdn())
                            .addingToSession(request,"sessionid",sessionId)
                            .addingToSession(request,"latestStamp",API.getCurrentStamp()+"");
                }
            };
            return wsResponse.thenApplyAsync(onSuccess,this.httpExecutionContext.current());
        }else{
            result.put("status",0);
            result.put("message","Wrong phone number or password");
            return CompletableFuture.completedFuture(ok(result));
        }
    }

    public CompletionStage<Result> getQuestions(Http.Request request) {

        ObjectNode result = Json.newObject();
        result.put("token",API.getCSRFToken(request));
        Form<LoginObject> loginObjectForm = loginRequestForm.bindFromRequest(request);
        LoginObject loginObject = loginObjectForm.get();
        logger.info(loginObject.getAnswer());
        logger.info(API.getMsisdn(request));
        logger.info(API.getSessionId(request));

        if(loginObject != null && loginObject.isValidLoginQuestionsObject()){
            WSRequest wsRequest = ws.url(INITIATE_URL);
            CompletionStage<? extends WSResponse> wsResponse =wsRequest
                    .addQueryParameter("input",loginObject.getAnswer())
                    .addQueryParameter("sessionid",API.getSessionId(request))
                    .addQueryParameter("msisdn",API.getMsisdn(request))
                    .addQueryParameter("country",COUNTRY)
                    .addQueryParameter("operator",OPERATOR)
                    .get();

            Function<WSResponse, Result> onSuccess = response -> {
                logger.info(response.getBody());

                if (response.getStatus() != 200) {
                    result.put("status",0);
                    result.put("message","Wrong phone number or password");
                    return ok(result);
                }else{
                    String responseBody = "Tafadhari weka kiasi:";
                    responseBody = response.getBody();
                    int displayNumber =getDisplayNumber(responseBody);
                    AnswerObject answerObject = getAnswers(responseBody);
                    result.put("status",1);
                    result.put("displayNumber",displayNumber);
                    result.put("message",responseBody);
                    result.put("answer1",answerObject.getAnswer1());
                    result.put("answer2",answerObject.getAnswer2());
                    return ok(result)
                            .addingToSession(request,"latestStamp",API.getCurrentStamp()+"");
                }
            };
            return wsResponse.thenApplyAsync(onSuccess,this.httpExecutionContext.current());
        }else{
            result.put("status",0);
            result.put("message","Wrong phone number or password");
            return CompletableFuture.completedFuture(ok(result));
        }
    }

    public CompletionStage<Result> continueQuestions(Http.Request request) {

        ObjectNode result = Json.newObject();
        String token =API.getCSRFToken(request);
        result.put("token",token);
        Form<LoginObject> loginObjectForm = loginRequestForm.bindFromRequest(request);
        LoginObject loginObject = loginObjectForm.get();
        if(loginObject != null && loginObject.isValidLoginQuestionsObject()){
            WSRequest wsRequest = ws.url(INITIATE_URL);
            String sessionId = API.getCurrentStamp()+"";
            CompletionStage<? extends WSResponse> wsResponse =wsRequest
                    .addQueryParameter("input",KEY_WORD)
                    .addQueryParameter("sessionid",API.getSessionId(request))
                    .addQueryParameter("msisdn",API.getMsisdn(request))
                    .addQueryParameter("country",COUNTRY)
                    .addQueryParameter("operator",OPERATOR)
                    .get();

            Function<WSResponse, Result> onSuccess = response -> {
                logger.info(response.getBody());

                if (response.getStatus() != 200) {
                    result.put("status",0);
                    result.put("message","Wrong phone number or password");
                    return ok(result);
                }else{
                    String responseBody = "Tafadhari weka kiasi:";
                    responseBody = response.getBody();
                    int displayNumber =getDisplayNumber(responseBody);
                    AnswerObject answerObject = getAnswers(responseBody);
                    result.put("status",1);
                    result.put("displayNumber",displayNumber);
                    result.put("message",responseBody);
                    result.put("answer1",answerObject.getAnswer1());
                    result.put("answer2",answerObject.getAnswer2());
                    return ok(result)
                            .addingToSession(request,"latestStamp",API.getCurrentStamp()+"");
                }
            };
            return wsResponse.thenApplyAsync(onSuccess,this.httpExecutionContext.current());
        }else{
            result.put("status",0);
            result.put("message","Wrong phone number or password");
            return CompletableFuture.completedFuture(ok(result));
        }
    }

    public Result test(Http.Request request) {
        ObjectNode result = Json.newObject();
        String responseBody = "Tafadhari weka kiasi:";
        responseBody = "Salio lako ni 100.00.\nOngeza salio kuendelea na Quiz, 200.00TZS kwa swali\n1.Ongeza\n2.Sitisha"; //works correctly
        //responseBody = "Uchaguzi wako si sahihi. Tafadhali chagua namba tu mfano: '1' SportsPesa Mchezaji bora 2019/20 ni?\n1.Clatous Chama\n2.Mbwana Samatta\nMfano: kwa 'Clatous Chama', Jibu : '1";
        /*responseBody = "Uchaguzi wako si sahihi. Tafadhali chagua namba tu mfano: '1'\n" +
                "SportsPesa Mchezaji bora 2019/20 ni?\n" +
                "1.Clatous Chama\n" +
                "2.Mbwana Samatta\n" +
                "\n" +
                "Mfano: kwa 'Clatous Chama', Jibu : '1'";*/
        /*responseBody = "Hongera sana, unapointi 1\n" +
                "Medie Kagere alijiunga na Simba akitokea timu gani?\n" +
                "1.GOR MAHIA FC\n" +
                "2.APR FC\n" +
                "\n" +
                "Mfano: kwa 'GOR MAHIA FC', Jibu : '1'";*/
        /*responseBody = "Jaribu tena, unapointi 0\n" +
                "Simba Sports Club imeshinda mara ngapi ubingwa wa CECAFA?\n" +
                "1.Mara 16\n" +
                "2.Mara 6\n" +
                "\n" +
                "Mfano: kwa 'Mara 16', Jibu : '1'";*/
        /*responseBody = "Jaribu tena, unapointi 0\n" +
                "Je!Simba ni timu ya kwanza Afrika Kushinda kombe la Klabu Bingwa?\n" +
                "1.Hapana\n" +
                "2.Ndiyo\n" +
                "\n" +
                "Mfano: kwa 'Hapana', Jibu : '1'";*/
        /*responseBody = "Hongera sana, unapointi 1\n" +
                "Haji Manara ni mchezaji wa Simba SC,JE anacheza nafasi gani ?\n" +
                "1.Hapana\n" +
                "2.Kiungo\n" +
                "\n" +
                "Mfano: kwa 'Hapana', Jibu : '1'";*/
        responseBody = "Hongera sana, unapointi 1\n" +
                "Simba Sports imeshinda mara ngapi Ubingwa wa Ligi Kuu ?\n" +
                "1.Mara 21\n" +
                "2.Mara 17\n" +
                "\n" +
                "Mfano: kwa 'Mara 21', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Mchezaji gani ameifunga Yanga magoli matatu “Hat-trick”?\n" +
                "1.Abdalla Kibaden\n" +
                "2.Emmanuel Okwi\n" +
                "\n" +
                "Mfano: kwa 'Abdalla Kibaden', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Mchezaji yupi kati ya hawa ameifunga Yanga mara nyingi Zaidi?\n" +
                "1.Mussa Mgosi\n" +
                "2.Emmanuel Okwi\n" +
                "\n" +
                "Mfano: kwa 'Mussa Mgosi', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Ipi idadi kubwa ya magoli ambayo Simba iliwahi kuifunga Yanga?\n" +
                "1.6-0\n" +
                "2.5-0\n" +
                "\n" +
                "Mfano: kwa '6-0', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Mwaka 2003 Simba Iliitoa timu gani kati ya zifuatazo na kuingia hatua ya Makundi ya Ligi ya Mabingwa Afrika?\n" +
                "1.Enyimba FC\n" +
                "2.Zamalek FC\n" +
                "\n" +
                "Mfano: kwa 'Enyimba FC', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Simba Sports Club imeshinda mara ngapi ubingwa wa CECAFA?\n" +
                "1.Mo Dewji\n" +
                "2.Mohammed Dewji\n" +
                "\n" +
                "Mfano: kwa 'Mo Dewji', Jibu : '1";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Kati ya majina yafuatayo, yapi yalikuwa majina ya kwanza kutumiwa na Simba SC kabla ya hili la sasa la Simba Sports Club ?\n" +
                "1.Eagles SC, Sunderland SC na Queens SC\n" +
                "2.Eagles SC, Sunderland SC na Msimbazi SC\n" +
                "\n" +
                "Mfano: kwa 'Eagles SC, Sunderland SC na Queens SC', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Kocha Mkuu wa Simba B (U20& U17) ni Sven Vandenbroeck ?\n" +
                "1.Ndiyo\n" +
                "2.Hapana\n" +
                "\n" +
                "Mfano: kwa 'Ndiyo', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Kati ya wafuatao, nani mchezaji bora wa Ligi kuu msimu wa 2019/20?\n" +
                "1.Clatous Chama\n" +
                "2.Medie Kagere\n" +
                "\n" +
                "Mfano: kwa 'Clatous Chama', Jibu : '1'";
        responseBody = "Jaribu tena, unapointi 0\n" +
                "Kocha yupi wa Simba SC kati ya wafuatao aliisaidia timu kufika mbali kwenye michuano ya Klabu bingwa CAF?\n" +
                "1.Paul W. Gwivaha\n" +
                "2.Patrick Aussems\n" +
                "\n" +
                "Mfano: kwa 'Paul W. Gwivaha', Jibu : '1'";
        responseBody = "Hongera sana, unapointi 1\n" +
                "Msimu wa 2018/19 Simba SC iliingia hatua ya Makundi kwenye Champions League Afrika kwa kuishinda timu gani kati ya hizi ?\n" +
                "1.Zesco FC\n" +
                "2.Nkana FC,\n" +
                "\n" +
                "Mfano: kwa 'Zesco FC', Jibu : '1'";
        int displayNumber =getDisplayNumber(responseBody);
        AnswerObject answerObject = getAnswers(responseBody);
        result.put("status",1);
        result.put("displayNumber",displayNumber);
        result.put("answer1",answerObject.getAnswer1());
        result.put("answer2",answerObject.getAnswer2());
        return ok(result);
    }

    public int getDisplayNumber(String responseBody){
        if(StringUtils.contains(responseBody,"Tafadhari weka kiasi")){
            return IS_SHOW_ENTER_AMOUNT;
        }else if(StringUtils.contains(responseBody,"kuthibitisha malipo")){
            return IS_SHOW_ENTER_PIN;
        }else if(StringUtils.contains(responseBody,"Asante")){
            return IS_SHOW_QUIT_FORM;
        }else{
            return IS_SHOW_NORMAL;
        }
    }

    public AnswerObject getAnswers(String responseBody){
        AnswerObject answerObject = new AnswerObject();
        try{
            String answer1 = StringUtils.substringBetween(responseBody,"\n1.","\n2.");
            String answer2 = StringUtils.substringBetween(responseBody,"\n2.","\n");
            if(answer2 == null){
                answer2 = StringUtils.substringAfterLast(responseBody,"\n2.");
            }
            answerObject.setAnswer1(answer1);
            answerObject.setAnswer2(answer2);
        }catch (Exception e){

        }
        return answerObject;
    }
}
